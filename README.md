# Setup WSL (Windows Subsystem for Linux)
#### Make sure your windows version is above `Microsoft Windows [Version 10.0.16299.15]`  
Run in CMD `var`

#### If your windows store is deactivated, do this steps  
1. Run PowerShell with administrator privileges  
2. Run in CMD `Get-Appxpackage -Allusers`
3. Copy the output into a notepad and search for e.g.   
`Microsoft.WindowsStore_11708.1001.21.0_x64__8wekyb3d8bbwe`  
4. REPLACE the packgename in this CMD command and run  
`Add-AppxPackage -register "C:\Program Files\WindowsApps\Microsoft.WindowsStore_11708.1001.21.0_x64__8wekyb3d8bbwe\AppxManifest.xml" -DisableDevelopmentMod`

#### Open the app-store and install WSL by this guide
https://docs.microsoft.com/en-us/windows/wsl/install-win10  
> You're done! Now you can use your Linux environment.

#### Update Distribution - run this commands
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```

#### Install Apache
```
sudo apt-get install apache2
sudo a2enmod proxy_fcgi setenvif rewrite ssl
```

Allow override all in `/etc/apache2/apache2.conf`
```
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```
Restart apache `sudo service apache2 restart`

#### Install PHP  

PHP 7.1  
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.1-fpm php7.1-mcrypt php7.1-mbstring php7.1-xml php7.1-mysql php7.1-curl php7.1-gd php7.1-imagic php7.1-zip php7.1-dev
sudo a2enconf php7.1-fpm
```  
PHP 7.3  
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.3
sudo apt install php7.3-cli php7.3-fpm php7.3-json php7.3-common php7.3-mysql php7.3-zip php7.3-gd  php7.3-mbstring php7.3-curl php7.3-xml php7.3-bcmath php7.3-imagick php-pear php7.3-dev
sudo apt-get install libmcrypt-dev libreadline-dev
sudo pecl install mcrypt-1.0.2 (press enert @ promt)
```

#### Install MySQL
```
sudo apt-get install mysql-server
```
(see also troubleshooting)  

#### Install PhpMyAdmin
`sudo apt-get install phpmyadmin` (if mysql-port error occures, choose ignore)  

Check if there is a file `phpmyadmin.conf` in `/etc/apache2/conf-available`  
If not, create a symlink `sudo ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf`  

Set mysql root password (replace MY_PASSWORD):
```
sudo mysql
use mysql;
update user set authentication_string=PASSWORD("MY_PASSWORD") where User='root';
flush privileges;
quit;
```

Set up phpmyadmin limits (adjust php-version):
Edit `/etc/php/7.1/fpm/php.ini` and `/etc/php/7.1/cli/php.ini` to adjust this values  
`memory_limit = 1024M`,  
`post_max_size = 2048M`,  
`upload_max_filesize = 2048M`,  
  
`max_input_time = 6000`,  
`max_execution_time = 6000`  

Adjust `/etc/apache2/sites-available/000-default.conf`
```
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.backup
sudo nano /etc/apache2/sites-available/000-default.conf
```
Insert into vhost config:  
```
Alias /phpmyadmin "/usr/share/phpmyadmin/"
<Directory "/usr/share/phpmyadmin/">
    Order allow,deny
    Allow from all
    Require all granted
</Directory>
```

Then restart -> `exit`  
Now phpmyadmin should be available at http://localhost/phpmyadmin/  

#### Install Node
Start your Console in Administrator mode  
 Install NPM  
```
sudo apt install nodejs
sudo apt install npm
apt-get install build-essential libssl-dev
```
Install NVM by https://github.com/nvm-sh/nvm/blob/master/README.md  
and restart Ubuntu CLI
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v[VERSION]/install.sh | bash
exit
```
Then install the current stable version of node and update npm:  
```
nvm install stable
npm update -g npm
```

#### Generate and Install SSL Certificates
Install certbaker `npm install -g certbaker`  
Generate a new certificate (replace with yor dev-domain)
```
certbaker b dev.example.com
```

After certbaker is executed first time, it creates a root-certificate at `/home/<UBUNTU_USER>/.certbaker/rootCA.crt`  

First create a temp folder (on Windows) `C:\temp` and copy the certificate from Ubuntu to Windows  
`sudo cp ~/.certbaker/rootCA.crt /mnt/c/temp/`  
Import the root certificate in `~/.certbaker/rootCA.crt` into your list of trusted root authorities on Windows.  
To do this press `Windows + R` and run `certmgr.msc`  
Then add the certificate by right-click on `Trusted certificates`/`Certificates` -> import
  
Add the root certificate also to Firefox.  

#### Mount / create and symlink Dev directory on Windows
Create a directory that will hold your code on windows e.g. `C:\tools\workspace\wsl\www`  
Now remove the existing `/var/www` directory in WSL and symlink that folder
```
sudo rm -Rf /var/www
sudo ln -s /mnt/c/tools/workspace/wsl/www /var/www
```

#### Create Virtual Hosts
Create a new virtual host config by adding a new config to `/etc/apache2/sites-available/`
```
sudo nano /etc/apache2/sites-available/dev.example.com.conf
```
Config content:  
replace/adjust all <UBUNTU_USER>, php-version, domain
```
<VirtualHost *:80>
    DocumentRoot "/var/www/dev.example.com/public"
    ServerName dev.example.com
</VirtualHost>
 
<VirtualHost *:443>
    DocumentRoot "/var/www/dev.example.com/public"
    ServerName dev.example.com
 
    <Directory /var/www/dev.example.com>
        AllowOverride All
        Require all granted
 
        <FilesMatch "\.php$">
            SetHandler "proxy:unix:/run/php/php7.1-fpm.sock|fcgi://localhost/"
        </FilesMatch>
    </Directory>
 
    SSLEngine on
    SSLCertificateFile /home/<UBUNTU_USER>/.certbaker/certificates/dev.example.com/dev.example.com.crt
    SSLCertificateKeyFile /home/<UBUNTU_USER>/.certbaker/certificates/dev.example.com/dev.example.com.key
</VirtualHost>
```
Now activate the the site with 
```
sudo a2ensite dev.example.com.conf
```
Don't forget to create the project-folder and the `public` folder in your mounted directory, then restart apache
```
sudo service apache2 restart
```
(see also troubleshooting)  

#### Add host to hosts file
Start any text-editor in Administrator mode and open `C:\Windows\System32\drivers\etc\hosts`  
append '127.0.0.1 localhost dev.example.com'  

#### Autostart WSL Services
The services will not start automaticly - so create an autostart script 
```
sudo nano /opt/startup.sh`
```
Script content (adjust php-version):
```
#!/bin/bash
 
service apache2 start &> /dev/null
service php7.1-fpm start &> /dev/null
service mysql start &> /dev/null
```
Then add execute permissions:
```
sudo chmod +x /opt/startup.sh
```
Note: The &-sign supresses the output of errors!  
  
Start up on windows-start  
Then create a new file wsl.bat in `C:\Users\<Windows_User>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`  
(adjust script - replace <Windows_User>)
```
:: You can start any terminal which invokes batch here, WSL doesn't yet support background tasks (comming soon) and therefore bash.exe needs to be runnig somehow before starting the services
:: This will probably be obsolete with the next major Windows update
C:\Users\<Windows_User>\AppData\Local\hyper\Hyper.exe
 
bash -c "echo <PASSWORD> | sudo -S /opt/startup.sh"
```

--- OR ---  

append to file `sudo nano ~/.bashrc`
```
sudo /opt/startup.sh
```
on session startup, you will asked for a password to run the script with sudo privileges  

#### Adding SSH Keys
Link your generated ssh-keys of windows .ssh folder to ubuntu  
```
sudo mkdir ~/.ssh
sudo ln -s /mnt/c/Users/<WINDOWS.USER>/.ssh/id_rsa.pub ~/.ssh/id_rsa.pub
```


# Extras

#### Handling npm/nvm versions
If you use a certain nvm version for the most projects, you can add this two lines to the startup-script  
which will perform nvm install - so you cant forget it - and avoid breaking node_modules by unsing npm run `script`
```
source ~/.nvm/nvm.sh
nvm install v8.4.0
```

#### Setting up php
Enable errors output by adding `display_errors = on` at the end of `/etc/php/7.1/fpm/php.ini`  
Install php-Xdebug e.g. to get a nice formatted var_dump() `sudo apt-get install php-xdebug`, then restart  

#### Adding Hyper Console
Install https://hyper.is/  
Then edit `C:\Users\<Windows_User>\.hyper.js` and set this value
```
shell: 'C:\\Windows\\System32\\bash.exe'
```

#### Troubleshooting
Apache2-error: `Protocol not available: AH00076: Failed to enable APR_TCP_DEFER_ACCEPT`  
if this erros/warning occueres while `sudo service apache2 restart` (ignore)  
> The TCP_DEFER_ACCEPT socket option is not natively supported by Windows.  
> WSL team are working with Windows networking team on this issue, but fix likely won't make it in RS3.  
> Doesn't block Apache2 from starting, just a warning. Pile-on below if otherwise.  
  
MySql-error: `... this is incompatible with sql_mode=only_full_group_by ...`  
Append to `/etc/mysql/my.cnf`  
```
[mysqld]
sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
```
  
npm error: `Node Sass could not find a binding for your current environment: Linux 64-bit with Node.js 9.x`  
run `npm rebuild node-sas` - if this does not work, `rm -rf node_modules`, then `npm install`